
import numpy as np
import torch
import torch.nn.functional as F


def callback_early_stopping(args, valid_accuracies):
    """
    Takes as argument the list with all the validation accuracies.
    If patience=k, checks if the mean of the last k accuracies is higher than the mean of the
    previous k accuracies (i.e. we check that we are not overfitting). If not, stops learning.
    @param valid_accuracies - list(float) , validation accuracy per epoch
    @return bool - if training stops or not
    @param args

    """
    epoch = len(valid_accuracies)

    # no early stopping for 2 * patience epochs
    if epoch // args.es_patience < 2:
        return False

    # Mean loss for last patience epochs and second-last patience epochs
    mean_previous = np.mean(valid_accuracies[epoch - 2 * args.es_patience:epoch - args.es_patience])
    mean_recent = np.mean(valid_accuracies[epoch - args.es_patience:epoch])
    delta = mean_recent - mean_previous
    if delta <= args.es_min_delta:
        if args.print_per_epoch:
            print("*CB_ES* Validation Accuracy didn't increase in the last %d epochs" % args.es_patience)
            print("*CB_ES* delta:", delta)
            print("callback_early_stopping signal received at epoch= %d" % len(valid_accuracies))
            print("Terminating training")
        return True
    else:
        return False


def eval_acc_kenn(y_true, y_pred):
    """ Accuracy function of KENN-Citeseer-Experiments"""
    correctly_classified = y_pred.squeeze() == y_true
    return torch.mean(correctly_classified.float())


def train_standard(model, data, optimizer, idx, criterion, range_constraint):
    """
    train_inductive
    training step for transductive setting
    @param model: callable NN model of torch.nn.Module
    @param data: data object with x, y, adjacency matrix (separate matrices for train/valid/test)
    @param optimizer: torch.optim object
    @param range_constraint object of RangeConstraint.py to constrain parameters
    @param idx: training indices
    @param criterion: loss function
    returns: loss (float)
    """
    model.train()
    optimizer.zero_grad()
    out = F.softmax(model(data.x[idx], data.edge_index, data.relations), dim=-1)
    loss = criterion(out, torch.argmax(data.y[idx], dim=1))
    loss.backward()
    optimizer.step()
    return loss.item()


@torch.no_grad()
def test_standard(model, data, idx, criterion):
    """
    test_inductive
    @param model - should be a NN of type torch.nn.module
    @param data - a PyG data object with x, y, adjacency matrix (for train, valid, test)
    @param idx - dictionary for split into train/valid/test
    return: accuracy (float) on train, valid, test set
    """
    model.eval()
    out = F.softmax(model(data.x[idx], data.edge_index, data.relations), dim=-1)
    loss = criterion(out, data.y[idx])
    acc = eval_acc_kenn(y_true=torch.argmax(data.y[idx], dim=1), y_pred=out.argmax(dim=-1, keepdim=True))

    return acc, loss


def train_kenn(model, data, optimizer, idx, criterion, range_constraint):
    """
    train_inductive
    training step for transductive setting
    @param model: callable NN model of torch.nn.Module
    @param data: data object with x, y, adjacency matrix (separate matrices for train/valid/test)
    @param optimizer: torch.optim object
    @param range_constraint object of RangeConstraint.py to constrain parameters
    @param idx: training indices
    @param criterion: loss function
    returns: loss (float)
    """
    model.train()
    optimizer.zero_grad()
    out = F.softmax(model(data.x, data.edge_index, data.relations), dim=-1)[idx]
    loss = criterion(out, data.y[idx])
    loss.backward()
    optimizer.step()
    model.apply(range_constraint)
    return loss.item()


@torch.no_grad()
def test_kenn(model, data, idx, criterion):
    """
    test_inductive
    @param model - should be a NN of type torch.nn.module
    @param data - a PyG data object with x, y, adjacency matrix (for train, valid, test)
    @param idx - dictionary for split into train/valid/test
    return: accuracy (float) on train, valid, test set
    """
    model.eval()
    out = F.softmax(model(data.x, data.edge_index, data.relations), dim=-1)[idx]
    loss = criterion(out, data.y[idx])
    acc = eval_acc_kenn(y_true=torch.argmax(data.y[idx], dim=1), y_pred=out.argmax(dim=-1, keepdim=True))

    return acc, loss
