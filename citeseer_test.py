
import argparse
import os
import pickle
from time import time
from KENN_Pytorch.RangeConstraint import RangeConstraint
from model import *
from preprocess_citeseer import load_and_preprocess
from training_citeseer import *
import torch
import torch_geometric
from datetime import datetime

def main():
    parser = argparse.ArgumentParser(description='Experiments')
    parser.add_argument('--dataset', type=str, default='citeseer')
    parser.add_argument('--device', type=int, default=0)
    parser.add_argument('--log_steps', type=int, default=10)
    parser.add_argument('--num_layers', type=int, default=5)
    parser.add_argument('--hidden_channels', type=int, default=50)
    parser.add_argument('--dropout', type=float, default=0.0)
    parser.add_argument('--lr', type=float, default=0.001)
    parser.add_argument('--epochs', type=int, default=300)  # default 300
    parser.add_argument('--runs', type=int, default=100)  # default 100
    parser.add_argument('--transductive', type=bool, default=True)
    parser.add_argument('--save_results', type=bool, default=True)
    parser.add_argument('--binary_preactivation', type=float, default=500.0)
    parser.add_argument('--num_kenn_layers', type=int, default=3)
    parser.add_argument('--range_constraint_lower', type=float, default=0)
    parser.add_argument('--range_constraint_upper', type=float, default=500)
    parser.add_argument('--es_enabled', type=bool, default=True)
    parser.add_argument('--es_min_delta', type=float, default=0.001)
    parser.add_argument('--es_patience', type=int, default=10)
    parser.add_argument('--seed', type=int, default=1234)
    parser.add_argument('--vd', type=float, default=0.2)
    parser.add_argument('--NUMBER_OF_CLASSES', type=int, default=6)
    parser.add_argument('--verbose', type=bool, default=False)
    parser.add_argument('--print_per_epoch', type=bool, default=True)
    parser.add_argument('--training_dimensions', type=list, default=[0.1, 0.25, 0.5, 0.75, 0.9])
    parser.add_argument('--adam_beta1', type=float, default=0.9)
    parser.add_argument('--adam_beta2', type=float, default=0.999)
    parser.add_argument('--adam_eps', type=float, default=1e-07)

    args = parser.parse_args()
    print(args)

    torch_geometric.seed_everything(args.seed)

    device = f'cuda:{args.device}' if torch.cuda.is_available() else 'cpu'
    print(f'Cuda available? {torch.cuda.is_available()}, Number of devices: {torch.cuda.device_count()}')

    if args.save_results:
        path = 'results'
        if not os.path.exists(path):
            os.makedirs(path)

    if args.transductive:

        print('Start Transductive Training')
        results_e2e_transductive = {}

        for td in args.training_dimensions:
            td_string = str(int(td * 100))
            print(' ========= Start training (' + td_string + '%)  =========')
            results_e2e_transductive.setdefault(td_string, {})

            test_accuracies_standard = []
            test_accuracies_kenn = []

            for run in range(args.runs):

                data, train_idx, valid_idx, test_idx = load_and_preprocess(args, 'transductive', td)

                # Standard Training
                range_constraint = RangeConstraint(lower=args.range_constraint_lower, upper=args.range_constraint_upper)
                model_standard = Standard(in_channels=data.num_features,
                                          out_channels=data.num_classes,
                                          hidden_channels=args.hidden_channels,
                                          num_layers=args.num_layers,
                                          dropout=args.dropout)

                optimizer = torch.optim.Adam(model_standard.parameters(), lr=args.lr, betas=(args.adam_beta1, args.adam_beta2), eps=args.adam_eps, amsgrad=False)
                criterion = torch.nn.CrossEntropyLoss()

                train_losses = []
                valid_losses = []
                train_accuracies = []
                valid_accuracies = []
                epoch_time = []

                for epoch in range(args.epochs):
                    start = time()
                    t_loss = train_standard(model_standard, data, optimizer, train_idx, criterion, range_constraint)
                    t_accuracy, _ = test_standard(model_standard, data, train_idx, criterion)
                    v_accuracy, v_loss = test_standard(model_standard, data, valid_idx, criterion)
                    end = time()

                    train_accuracies.append(t_accuracy)
                    valid_accuracies.append(v_accuracy)
                    train_losses.append(t_loss)
                    valid_losses.append(v_loss)
                    epoch_time.append(end - start)

                    if epoch % args.log_steps == 0 and args.print_per_epoch:
                        print(f'Run: {run + 1:02d}, '
                              f'Epoch: {epoch:02d}, '
                              f'Training loss: {t_loss:.10f}, '
                              f'Validation loss: {v_loss:.10f} '
                              f'Train Acc: {t_accuracy:.10f}, '
                              f'Valid Acc: {v_accuracy:.10f} ')

                    # early stopping
                    if args.es_enabled and callback_early_stopping(args, valid_accuracies):
                        break

                test_accuracy_standard, _ = test_standard(model_standard, data, test_idx, criterion)

                if args.print_per_epoch:
                    print(f'Test Accuracy: {test_accuracy_standard:.10f}, Training dimension: {td}')

                test_accuracies_standard.append(test_accuracy_standard)

                results_e2e_transductive[td_string].setdefault('Standard', []).append({
                    "train_losses": train_losses,
                    "train_accuracies": train_accuracies,
                    "valid_losses": valid_losses,
                    "valid_accuracies": valid_accuracies,
                    "test_accuracy": test_accuracy_standard,
                    "epoch_time": np.average(epoch_time),
                    "clause_weights": None
                })

                # KENN_Standard Training
                range_constraint = RangeConstraint(lower=args.range_constraint_lower, upper=args.range_constraint_upper)

                model_kenn = KENN_Standard(knowledge_file='knowledge_base',
                                           in_channels=data.num_features,
                                           out_channels=data.num_classes,
                                           hidden_channels=args.hidden_channels,
                                           num_layers=args.num_layers,
                                           num_kenn_layers=args.num_kenn_layers,
                                           dropout=args.dropout)

                optimizer = torch.optim.Adam(model_kenn.parameters(), lr=args.lr, betas=(args.adam_beta1, args.adam_beta2), eps=args.adam_eps, amsgrad=False)
                criterion = torch.nn.CrossEntropyLoss()

                train_losses = []
                valid_losses = []
                train_accuracies = []
                valid_accuracies = []
                epoch_time = []

                clause_weights_dict = {f"clause_weights_{i}": [] for i in range(args.num_kenn_layers)}

                for epoch in range(args.epochs):

                    start = time()
                    t_loss = train_kenn(model_kenn, data, optimizer, train_idx, criterion, range_constraint)
                    t_accuracy, _ = test_kenn(model_kenn, data, train_idx, criterion)
                    v_accuracy, v_loss = test_kenn(model_kenn, data, valid_idx, criterion)
                    end = time()

                    train_accuracies.append(t_accuracy)
                    valid_accuracies.append(v_accuracy)
                    train_losses.append(t_loss)
                    valid_losses.append(v_loss)
                    epoch_time.append(end - start)

                    for i in range(args.num_kenn_layers):
                        clause_weights_dict[f"clause_weights_{i}"].append(
                            [ce.clause_weight for ce in model_kenn.kenn_layers[i].binary_ke.clause_enhancers])

                    if epoch % args.log_steps == 0 and args.print_per_epoch:
                        print(f'Run: {run + 1:02d}, '
                              f'Epoch: {epoch:02d}, '
                              f'Training Loss: {t_loss:.6f}, '
                              f'Time: {end - start:.6f} '
                              f'Train: {t_accuracy:.6f}, '
                              f'Valid: {v_accuracy:.6f} ')

                    # early stopping
                    if args.es_enabled and callback_early_stopping(args, valid_accuracies):
                        break

                test_accuracy_kenn, _ = test_kenn(model_kenn, data, valid_idx, criterion)
                if args.print_per_epoch:
                    print(f'Test Accuracy: {test_accuracy_kenn:.10f}, Training dimension: {td} ')

                test_accuracies_kenn.append(test_accuracy_kenn)

                results_e2e_transductive[td_string].setdefault('KENN_Standard', []).append({
                    "train_losses": train_losses,
                    "train_accuracies": train_accuracies,
                    "valid_losses": valid_losses,
                    "valid_accuracies": valid_accuracies,
                    "test_accuracy": test_accuracy_kenn,
                    "epoch_time": np.average(epoch_time),
                    "clause_weights": clause_weights_dict
                })

            print(
                f'avg test accuracies Standard over {len(test_accuracies_standard)} iterations, td{td}: {np.average(test_accuracies_standard)}')
            print(
                f'max test accuracies Standard over {len(test_accuracies_standard)} iterations, td {td}: {np.max(test_accuracies_standard)}')
            print(
                f'min test accuracies Standard over {len(test_accuracies_standard)} iterations, td {td}: {np.min(test_accuracies_standard)}')
            print(
                f'std test accuracies Standard over {len(test_accuracies_standard)} iterations, td {td}: {np.std(test_accuracies_standard)}')

            print(
                f'avg test accuracies KENN over {len(test_accuracies_kenn)} iterations, td {td}: {np.average(test_accuracies_kenn)}')
            print(
                f'max test accuracies KENN over {len(test_accuracies_kenn)} iterations, td {td}: {np.max(test_accuracies_kenn)}')
            print(
                f'min test accuracies KENN over {len(test_accuracies_kenn)} iterations, td {td}: {np.min(test_accuracies_kenn)}')
            print(
                f'std test accuracies KENN over {len(test_accuracies_kenn)} iterations, td {td}: {np.std(test_accuracies_kenn)}')

        if args.save_results:
            with open('results/config.txt', 'w') as f:
                f.write(str(args))
                f.write('timestamp ' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))
            with open('./results/results_transductive_{}runs'.format(args.runs), 'wb') as output:
                pickle.dump(results_e2e_transductive, output)


if __name__ == "__main__":
    main()
