""" Base Neural Network and Knowledge Enhanced Models """

import torch
import torch.nn.functional as F
from parsers import *


class Standard(torch.nn.Module):
    """ Original Base Neural Network by KENN paper """

    # in_channels = 3703
    # out_channels = 6
    # hidden_channels = 50

    def __init__(self, in_channels, hidden_channels, out_channels, num_layers,
                 dropout):
        super().__init__()
        self.name = 'Standard'
        self.lin_layers = torch.nn.ModuleList()
        self.lin_layers.append(torch.nn.Linear(3703, 50))
        self.lin_layers.append(torch.nn.Linear(50, 50))
        self.lin_layers.append(torch.nn.Linear(50, 50))
        self.lin_layers.append(torch.nn.Linear(50, out_channels))
        self.dropout = dropout
        self.training = True

        # initialize weights
        for lin in self.lin_layers:
            torch.nn.init.xavier_uniform(lin.weight)
            torch.nn.init.zeros_(tensor=lin.bias)

    def forward(self, x, edge_index, relations):
        for i, lin in enumerate(self.lin_layers[:-1]):
            x = torch.matmul(x, torch.transpose(torch.Tensor(lin.weight), 0, 1)) + lin.bias
            x = F.relu(x)
            F.dropout(x, p=self.dropout, training=self.training)
        x = torch.matmul(x, torch.transpose(torch.Tensor(self.lin_layers[-1].weight), 0, 1)) + self.lin_layers[-1].bias
        return x


class KENN_Standard(Standard):
    """ KENN with MLP (from ogb) as base NN"""

    def __init__(self, knowledge_file, hidden_channels, in_channels, out_channels, num_layers, num_kenn_layers, dropout,
                 explainer_object=None):
        super().__init__(in_channels=in_channels, out_channels=out_channels,
                         hidden_channels=hidden_channels,
                         num_layers=num_layers, dropout=dropout)
        self.name = str('KENN_' + self.name)
        self.knowledge_file = knowledge_file
        self.explainer_object = explainer_object
        self.kenn_layers = torch.nn.ModuleList()

        for _ in range(num_kenn_layers):
            self.kenn_layers.append(relational_parser(knowledge_file=knowledge_file, explainer_object=explainer_object))

    def forward(self, x, edge_index, relations):
        # call base NN
        z = super().forward(x, edge_index, relations)

        # call KENN layers
        for layer in self.kenn_layers:
            z, _ = layer(unary=z, edge_index=edge_index, binary=relations)

        return z
