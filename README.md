# On the Replicability of Knowledge Enhanced Neural Networks in a Graph Neural Network Framework

This repository contains our replication of the experiments conducted with [Knowledge Enhanced Neural Networks (KENN)](https://github.com/rmazzier/KENN-Citeseer-Experiments) [1] on the [Citeseer Dataset](https://linqs.soe.ucsc.edu/data) as well as our reinterpretation of the KENN model in PyTorch. Our submitted paper can be found here: [https://hal.inria.fr/hal-03692362]

KENN has been developed by Alessandro Daniele, Riccardo Mazzieri and Luciano Serafini.

In a perspective of extending the model to Graph Neural Networks and large-scale graphs, we make use the libraries [PyTorch Geometric](https://github.com/pyg-team/pytorch_geometric) [2] and [PyTorch Scatter](https://github.com/rusty1s/pytorch_scatter). 


## Overview 

In order to extend Knowledge Enhanced Neural Networks, we investigate the replicability of the approach and present a re-implementation of Knowledge Enhanced Neural Networks based on a Graph Neural Network framework (PyTorch Geometric). Knowledge Enhanced Neural Networks integrate prior knowledge in the form of logical formulas into an Artificial Neural Network by adding additional Knowledge Enhancement layers. The obtained results show that the model outperforms pure neural models as well as Neural-Symbolic models. Our long term goal is to be able to address more complex and large-scale knowledge graphs and to benefit from the wide range of functionalities available in PyTorch Geometric. To ensure that our implementation produces the same results, we replicate the original transductive experiments.

More information about KENN can be found in [1]. 

## How to Reproduce the Citeseer Experiments in PyTorch  

1. In order to make sure that the right environment is used, the necessary Python packages and their versions are specified in `requirements.txt`. We use Python 3.8. To install them go in the project directory and run the terminal command: 
```
pip install -r requirements.txt
```

2. To run the transductive Experiments on Citeseer with the default parameters specified in [2], run the following command from the project directory. 
```
python citeseer_test.py
```

The results will be stored in the `results` directory as `results_transductive_100runs` for example. 

3. The experimental setting and parameters of the model can be specified in the command line. In details, the following parameters are set. An overview of the parameters of the last conducted run is stored as `config.txt` in the `results` directory. 
```
--log_steps	                        Frequency of prints per epoch, default=10;
--num_layers				Number of layers in the Base NN, default=5;
--hidden_channels			Number of hidden units in the dense layers of Base NN, default=50;
--dropout				Dropout Rate in the Base NN, default=0.0;
--lr					Learning rate, default=0.001;
--epochs				Number of epochs per run, default=300;
--runs					Number of independent runs default=100;
--binary_preactivation			Preactivation of binary predicates, default=500.0;
--num_kenn_layers			Number of KENN layers, default=3;
--range_constraint_lower		Lower limit of clause weights, default=0;
--range_constraint_upper		Upper limit of clause weights, default=500;
--es_enabled				Activation of Early Stopping, default=True;
--es_min_delta				Early Stopping Minimum Delta, default=0.001;
--es_patience				Early Stopping Patience, default=10;
--seed					Random seed, default=0;
--vd					Validation set size in %, default=0.2;
--training_dimensions			Training set sizes in %, default=[0.1, 0.25, 0.5, 0.75, 0.9];
--adam_beta1 				Adam Optimizer Beta1, default=0.9;
--adam_beta2 				Adam Optimizer Beta2, default=0.999;
--adam_eps 				Adam Optimizer Epsilon, default=1e-07
```

In order to start the experiments with 200 runs and 5 kenn layers for example, run the command:

```
python citeseer_test.py --runs 200 --num_kenn_layers 5
```

4. The stored results can be printed by running the command
```
python evaluate.py 
```

By default, the file `results_transductive_100runs` is evaluated. If another results file should be inspected, run
```
python evaluate.py --file [name]
```

## References
[1] A. Daniele, L. Serafini, Neural networks Enhancement with logical knowledge, 2020. URL: https://arxiv.org/abs/2009.06087. doi:10.48550/ ARXIV.2009.06087.

[2] M. Fey, J. E. Lenssen, Fast graph representation learning with PyTorch Geometric, in: ICLR Work- shop on Representation Learning on Graphs and Manifolds, 2019.

This is a work of the [Tyrex Team](https://tyrex.inria.fr/). 
In case of comments or questions, feel free to [email us](luisa.werner@inria.fr).
