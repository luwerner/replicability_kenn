import argparse
import os
import numpy as np
import pandas as pd
import torch
from torch import cat, ones
from torch_geometric.data import Data


def unary_index(ids_list, _id):
    """
    Returns the index of the id inside ids_list. If multiple indexes are found, throws an error,
    and if no index is found, raises an Exception. If only one is found, returns it.

    Parameters:
    - ids_list = the list of all the ids;
    - id = the specific id we want to know the index of."""
    match = np.where(ids_list == _id)[0]

    assert len(match) < 2

    if len(match) == 0:
        raise Exception(_id)
    else:
        return match[0]


def generate_indexes_inductive(relations, ids_list, samples_in_training, samples_in_valid, verbose=True):
    """Generate the indexes to be used by kenn for the relational part for the Inductive learning task
    Specifically, this function returns the index couples (s1,s2) of all the edges for the inductive case:
    i.e. we remove edges (n1,n2) s.t. n1 is in the training set, and n2 is in the test set.

    Parameters:
    :param samples_in_valid
    :param verbose
    :param relations: array containing the edges of the graph. The format is [cited_paper, citing paper]
    :param ids_list: array containing the ids of all the samples
    :param samples_in_training: n. of samples in the training set. Needed to tell if an index refers to a
    sample of the training set or of the test set.
    """

    s1_training = []
    s2_training = []
    s1_valid = []
    s2_valid = []
    s1_test = []
    s2_test = []

    # iterate over the length of the number of edges
    deleted_edges = 0
    for i in range(len(relations)):
        try:
            # here we fetch the index of the FIRST element of the i-th edge
            match1 = unary_index(ids_list, relations[i, 0])
            # here we fetch the index of the SECOND element of the i-th edge
            match2 = unary_index(ids_list, relations[i, 1])

            # Here we decide if we want to put the found indexes in s*_training or in s*_test or s*_valid
            if match1 < samples_in_training and match2 < samples_in_training:
                # Both in training set
                s1_training.append([match1])
                s2_training.append([match2])
                # both in validation set
            elif match1 in range(samples_in_training, samples_in_training + samples_in_valid) and match2 in range(
                    samples_in_training, samples_in_training + samples_in_valid):
                # offset removed, because later we use the full frame for predictions and select indices then
                # s1_valid.append([match1 - samples_in_training])
                # s2_valid.append([match2 - samples_in_training])
                s1_valid.append([match1])
                s2_valid.append([match2])
            elif match1 >= (samples_in_training + samples_in_valid) and match2 >= (
                    samples_in_training + samples_in_valid):
                # Both in test set
                # s1_test.append([match1 - (samples_in_training + samples_in_valid)])
                # s2_test.append([match2 - (samples_in_training + samples_in_valid)])
                s1_test.append([match1])
                s2_test.append([match2])
            else:
                deleted_edges += 1
        except Exception as e:
            if verbose:
                print('Missing paper, removed citation! Id: ')
                print(e)

    print(f'Deleted {deleted_edges} edges ')

    if len(s1_training) == 0 or len(s1_valid) == 0 or len(s1_test) == 0:
        print('one of the edge lists for the subsets is empty')

    return np.array(s1_training).astype(np.int32), np.array(s2_training).astype(np.int32), \
           np.array(s1_valid).astype(np.int32), np.array(s2_valid).astype(np.int32), \
           np.array(s1_test).astype(np.int32), np.array(s2_test).astype(np.int32)


def generate_indexes_transductive(relations, ids_list, verbose=True):
    """Generate the indexes to be used by kenn for the relational part, for the Transductive learning task.
    Specifically, this function returns the index couples (s1,s2) of all the edges for the transductive case:
    i.e. we don't remove edges (n1,n2) s.t. n1 is in the training set, and n2 is in the test set.

    Parameters:
    - relations: np.array containing the edges of the graph. The format is [cited_paper, citing paper];
    - ids_list: np.array containing the ids of all the samples"""
    s1 = []
    s2 = []

    for i in range(len(relations)):
        try:
            # here we fetch the index of the FIRST element of the i-th edge
            match1 = unary_index(ids_list, relations[i, 0])
            # here we fetch the index of the SECOND element of the i-th edge
            match2 = unary_index(ids_list, relations[i, 1])

            s1.append([match1])
            s2.append([match2])
        except Exception as e:
            if verbose:
                print('Missing paper, removed citation! Id: ')
                print(e)
    # we return s1 and s2 as np.arrays and as column vectors (shape: (len(relations),1))
    return np.array(s1).astype(np.int32), np.array(s2).astype(np.int32)


def permute(y, samples_per_class, args):
    """Return a permutation of the dataset indexes, with samples_per_class samples for each class in the first rows.
    The first rows are used outside as a training set (which must be balanced)

    Parameters:
    - y = the pd.Series containing all the classes of all the samples
    - samples_per_class = int representing the number of samples we want to keep for each class when
      balancing the dataset.
    """
    # List of classes
    # classes = sorted(list(set(y))) (for reproducible preprocessing - set is not deterministic )
    classes = list(set(y))

    # initialize empty arrays, will be filled below
    p_train = np.array([], dtype=np.int32)
    p_valid = np.array([], dtype=np.int32)
    p_other = np.array([], dtype=np.int32)

    for c in classes:
        # the [0] is because np.where returns a tuple with an array inside, and we want the array
        # i_c contains the indexes of samples in the dataset of class c
        i_c = np.where(y == c)[0]
        # generate a shuffled series of indexes
        p = np.random.permutation(len(i_c))
        # shuffle i_c according to the shuffled indexes we generated
        i_c = i_c[p]

        # Here we fill up the empty arrays initialized above (with np.concatenate).
        # Here we select only the desired amount of samples for each class
        p_train = np.concatenate((p_train, i_c[:samples_per_class]), axis=0)
        p_other = np.concatenate((p_other, i_c[samples_per_class:]), axis=0)

    p_train = np.random.permutation(p_train)
    return np.concatenate((p_train, np.random.permutation(p_other)), axis=0)


def get_train_and_valid_lengths(features, td, args):
    """
    Given the array with the whole dataset, and the percentage of the training set,
    returns the lengths of the training set and validation set, such that the training set
    is balanced and that the validation set is s.VALIDATION_PERCENTAGE * train_length
    """
    total_number_of_samples = features.shape[0]
    t_all = int(round(total_number_of_samples * td))
    number_of_samples_training = int(round(t_all * (1. - args.vd)))

    # Since we want a balanced training dataset (i.e. same number of samples of the same class, for each class)
    # we define how many samples per class we want.
    samples_per_class = int(round(number_of_samples_training / args.NUMBER_OF_CLASSES))

    number_of_samples_training = samples_per_class * args.NUMBER_OF_CLASSES
    number_of_samples_validation = t_all - number_of_samples_training
    return number_of_samples_training, number_of_samples_validation


def generate_knowledge(classes):
    # Generate knowledge
    kb = ''

    # List of predicates
    for c in classes:
        kb += c + ','

    kb = kb[:-1] + '\nCite\n\n'

    # No unary clauses

    kb = kb[:-1] + '\n>\n'

    # Binary clauses

    # nC(x),nCite(x.y),C(y)
    for c in classes:
        kb += '_:n' + c + '(x),nCite(x.y),' + c + '(y)\n'

    with open('knowledge_base', 'w') as kb_file:
        kb_file.write(kb)


def load_and_preprocess(args, mode, td):
    """
    :param args
    :param mode: inductive or transductive
    :param td: training dimension (how much data is used for training)
    """

    citeseer = pd.read_csv('data/citeseer.content', delimiter='\t', header=None)

    # First column contains indexes, last one the label. In the middle, the features
    # ids: id of each paper (shape: (3312,1));
    # x: the features for each paper (shape:(3312, 3703));
    # y: the class for each paper (shape:(3312,1)).
    ids, x, y = np.split(citeseer, [1, -1], axis=1)

    train_len, valid_len = get_train_and_valid_lengths(x, td, args)

    train_idx = range(train_len)
    valid_idx = range(train_len, train_len + valid_len)
    test_idx = range(train_len + valid_len, x.shape[0])

    p = permute(y.iloc[:, 0], int(train_len // args.NUMBER_OF_CLASSES), args)

    # balances and shuffles ids and x
    ids = ids.to_numpy().astype(str)[p, :]
    x = x.to_numpy().astype(float)[p, :]

    y = pd.get_dummies(y, prefix=['class'])
    # just gets a list with the names of all the possible classes
    classes = [c[5:] for c in list(y.columns)]

    generate_knowledge(classes)

    # balances and shuffles y
    y = y.to_numpy().astype(np.float32)[p, :]
    relations = pd.read_csv('data/citeseer.cites', delimiter='\t', header=None).to_numpy()

    if mode == 'transductive':
        s1, s2 = generate_indexes_transductive(relations, ids, verbose=args.verbose)
        s1, s2 = torch.LongTensor(s1), torch.LongTensor(s2)
        edge_index = torch.transpose(cat([s1, s2], dim=1), 0, 1)
        b = ones(len(s1), dtype=torch.float32).unsqueeze(dim=1) * 500
        data = Data(x=torch.Tensor(x), y=torch.Tensor(y), edge_index=edge_index, relations=b, num_classes=len(classes))

        return data, train_idx, valid_idx, test_idx

    if mode == 'inductive':
        s1tr, s2tr, s1val, s2val, s1te, s2te = generate_indexes_inductive(relations, ids, train_len,
                                                                          samples_in_valid=valid_len,
                                                                          verbose=args.verbose)
        s1tr, s2tr, s1val, s2val, s1te, s2te = torch.LongTensor(s1tr), torch.LongTensor(s2tr), torch.LongTensor(
            s1val), torch.LongTensor(s2val), torch.LongTensor(s1te), torch.LongTensor(s2te)
        btr = ones(len(s1tr), dtype=torch.float32).unsqueeze(dim=1) * 500
        bval = ones(len(s1val), dtype=torch.float32).unsqueeze(dim=1) * 500
        bte = ones(len(s1te), dtype=torch.float32).unsqueeze(dim=1) * 500

        # Now create a PyG object out of this
        # here: indices refer to the full set.
        # Relations in train must be in train_indices, for valid in valid_indices etc...
        # (first samples train, then valid, then test)
        data_train = Data(x=torch.Tensor(x), y=torch.Tensor(y),
                          edge_index=torch.transpose(cat([s1tr, s2tr], dim=1), 0, 1), relations=btr,
                          num_classes=len(classes), num_features=x.shape[1])
        data_valid = Data(x=torch.Tensor(x), y=torch.Tensor(y),
                          edge_index=torch.transpose(cat([s1val, s2val], dim=1), 0, 1), relations=bval,
                          num_classes=len(classes), num_features=x.shape[1])
        data_test = Data(x=torch.Tensor(x), y=torch.Tensor(y),
                         edge_index=torch.transpose(cat([s1te, s2te], dim=1), 0, 1), relations=bte,
                         num_classes=len(classes), num_features=x.shape[1])

        return data_train, data_valid, data_test, train_idx, valid_idx, test_idx

    else:
        print('ArgumentError: Wrong mode specified. Choose transductive or inductive')
        return None

